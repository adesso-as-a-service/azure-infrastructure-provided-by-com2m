
#!/bin/bash
SERVER=wallbehub.azurecr.io
USERNAME=wallbe-acr-username
PASSWORD=wallbe-acr-password
MAIL=info@com2m.de
NAMESPACE=default
  
  
kubectl create secret docker-registry wallbe-hub-docker-registry-secret --docker-server=${SERVER} --docker-username=${USERNAME} --docker-password=${PASSWORD} --docker-email=${MAIL} --namespace=${NAMESPACE}