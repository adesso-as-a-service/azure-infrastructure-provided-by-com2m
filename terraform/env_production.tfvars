azure_tenant_id                       = "e8bd4ad0-8137-4049-8a74-71426cdde849"

azure_subscription_id                = "80f1706b-5bff-46fc-9edb-9d7f8b941efb"

azure_location                        = "West Europe"

project_name                          = "wallbe-hub"

project_environment                   = "production"

aks_sku                               = "Paid"

aks_k8s_version                       = "1.21.2"

aks_k8s_node_version                  = "1.20.9"

aks_node_count                        = 3

aks_node_osdisk_size                  = 64

aks_node_vm_size                      = "Standard_D4as_v4"

aks_vnet_address_space                = "10.240.0.0/16"

aks_enable_log_analytics              = true

log_analytics_retention_in_days       = 90

storage_account_granted_ips           = []

storage_create_snapshots              = true

matomo_mysql_version                  = "8.0"

matomo_mysql_sku_name                 = "B_Gen5_1"

matomo_mysql_storage                  = "5120"

monitoring_business_incidents_webhook = "https://o365adessogroup.webhook.office.com/webhookb2/57dae572-440d-40a7-84c5-0be25993adbe@3d355765-67d9-47cd-9c7a-bf31179f56eb/IncomingWebhook/c61ff0a3f11140b39439e1f3ed06926c/4651529c-812f-432c-aaac-c4fd8bd25e09"
