terraform {
  required_version = ">= 0.14.6"

  required_providers {
    azurerm = {
      version = "2.82.0"
    }
    helm = {
      version = "1.3.2"
    }
    kubernetes = {
      version = "1.13.3"
    }
  }
  
  backend "azurerm" {
    tenant_id            = "e8bd4ad0-8137-4049-8a74-71426cdde849"
    subscription_id      = "80f1706b-5bff-46fc-9edb-9d7f8b941efb"
    resource_group_name  = "wallbe-hub-infrastructure"
    storage_account_name = "wallbehubinfrastructure"
    container_name       = "wallbe-hub-terraform"
    key                  = "tfstate"
  }
}

provider "azurerm" {
  subscription_id = var.azure_subscription_id
  tenant_id       = var.azure_tenant_id

  features {}
}

provider "helm" {
  kubernetes {
    host                   = azurerm_kubernetes_cluster.aks.kube_config[0].host
    cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config[0].cluster_ca_certificate)
    client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config[0].client_certificate)
    client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config[0].client_key)
  }
}

provider kubernetes {
  host                   = azurerm_kubernetes_cluster.aks.kube_config[0].host
  cluster_ca_certificate = base64decode(azurerm_kubernetes_cluster.aks.kube_config[0].cluster_ca_certificate)
  client_certificate     = base64decode(azurerm_kubernetes_cluster.aks.kube_config[0].client_certificate)
  client_key             = base64decode(azurerm_kubernetes_cluster.aks.kube_config[0].client_key)
}

locals {
  # Workaround to validate that workspace and project_environment are the same
  assert_workspace_and_project_environment_equals = var.project_environment != terraform.workspace ? file("ERROR: workspace must be equals to project_environment.") : null
}

resource "azurerm_resource_group" "aks" {
  name     = "${var.project_name}-${var.project_environment}"
  location = var.azure_location
}

resource "azurerm_container_registry" "acr" {
  count               = var.project_environment == "production" ? 1 : 0
  name                = replace(var.project_name, "-", "")
  resource_group_name = "wallbe-hub-infrastructure"
  location            = var.azure_location
  sku                 = "Standard"
}

resource "azurerm_role_assignment" "aks-resource-group_network-contributor" {
  scope                = azurerm_resource_group.aks.id
  role_definition_name = "Network Contributor"
  principal_id         = azurerm_kubernetes_cluster.aks.identity[0].principal_id
}

resource "azurerm_public_ip" "aks" {
  name                    = "${var.project_name}-${var.project_environment}-public-ip"
  resource_group_name     = azurerm_resource_group.aks.name
  location                = azurerm_resource_group.aks.location
  sku                     = "Standard"
  allocation_method       = "Static"
  idle_timeout_in_minutes = 20
  domain_name_label       = "${var.project_name}-${var.project_environment}"
}

resource "azurerm_public_ip" "aks-outbound" {
  name                    = "${var.project_name}-${var.project_environment}-public-outbound-ip"
  resource_group_name     = azurerm_resource_group.aks.name
  location                = azurerm_resource_group.aks.location
  sku                     = "Standard"
  allocation_method       = "Static"
}

resource "azurerm_public_ip" "aks-ocpp-1-6" {
  count                   = var.project_environment == "playground" ? 0 : 1
  name                    = "${var.project_name}-${var.project_environment}-public-ip-ocpp-1-6"
  resource_group_name     = azurerm_resource_group.aks.name
  location                = azurerm_resource_group.aks.location
  sku                     = "Standard"
  allocation_method       = "Static"
  idle_timeout_in_minutes = 20
}

resource "azurerm_public_ip" "aks-ocpp-1-6-alt" {
  count                   = var.project_environment == "playground" ? 0 : 1
  name                    = "${var.project_name}-${var.project_environment}-public-ip-ocpp-1-6-alt"
  resource_group_name     = azurerm_resource_group.aks.name
  location                = azurerm_resource_group.aks.location
  sku                     = "Standard"
  allocation_method       = "Static"
  idle_timeout_in_minutes = 20
}

resource "azurerm_log_analytics_workspace" "aks" {
  count               = var.aks_enable_log_analytics ? 1 : 0
  name                = "${var.project_name}-${var.project_environment}-log-analytics-workspace"
  resource_group_name = azurerm_resource_group.aks.name
  location            = azurerm_resource_group.aks.location
  retention_in_days   = var.log_analytics_retention_in_days
}


resource "azurerm_monitor_action_group" "aks" {
  count               = var.aks_enable_log_analytics ? 1 : 0
  name                = "${var.project_environment}-business-incidents-action"
  resource_group_name = azurerm_resource_group.aks.name
  short_name          = "BusIncidents"

  webhook_receiver {
    name                    = "${var.project_environment}-business-incidents-webhook"
    service_uri             = var.monitoring_business_incidents_webhook
    use_common_alert_schema = true
  }
}

resource "azurerm_monitor_scheduled_query_rules_alert" "aks-monitor-no-running-operating-costs-job" {
  count               = var.aks_enable_log_analytics ? 1 : 0
  name                = "${var.project_environment}-monitor-no-running-operating-costs-job"
  location            = azurerm_resource_group.aks.location
  resource_group_name = azurerm_resource_group.aks.name

  enabled        = true
  description    = "Alert when no operating costs job is run"
  data_source_id = azurerm_log_analytics_workspace.aks[0].id

  query       = <<-QUERY
let containerIds = ContainerInventory | where Image == "wallbe-billing-service" | distinct ContainerID;
ContainerLog
| where dayofmonth(now()) == 3
| where ContainerID in (containerIds)
| where LogEntry has "Calculating monthly operating cost"
| extend processingStatus = extract("exit code ([A-Z]+)", 1, LogEntry)
| where processingStatus == "SUCCESS"
| summarize Count = count() by processingStatus
| where Count < 1
  QUERY
  
  severity    = 1
  frequency   = 1440
  time_window = 2880
  trigger {
    operator  = "GreaterThan"
    threshold = 0
  }
  action {
    action_group = [azurerm_monitor_action_group.aks[0].id]
  }
}

resource "azurerm_monitor_scheduled_query_rules_alert" "aks-monitor-failed-operating-costs-job" {
  count               = var.aks_enable_log_analytics ? 1 : 0
  name                = "${var.project_environment}-monitor-failed-operating-costs-job"
  location            = azurerm_resource_group.aks.location
  resource_group_name = azurerm_resource_group.aks.name

  enabled        = true
  description    = "Alert when operating costs job is failed"
  data_source_id = azurerm_log_analytics_workspace.aks[0].id
  
  query       = <<-QUERY
let containerIds = ContainerInventory | where Image == "wallbe-billing-service" | distinct ContainerID;
ContainerLog
| where ContainerID in (containerIds)
| where LogEntry has "Failed to generate operating cost accounting settlement"
| extend tenantId = extract("tenant ([0-9a-zA-z_]+)", 1, LogEntry)
| summarize Count = count() by tenantId
| where Count > 0
  QUERY

  severity    = 1
  frequency   = 1440
  time_window = 2880
  trigger {
    operator  = "GreaterThan"
    threshold = 0
  }
  action {
    action_group = [azurerm_monitor_action_group.aks[0].id]
  }
}

resource "azurerm_monitor_scheduled_query_rules_alert" "aks-monitor-certificate-cron-job" {
  count               = var.aks_enable_log_analytics ? 1 : 0
  name                = "${var.project_environment}-monitor-certificate-cron-job"
  location            = azurerm_resource_group.aks.location
  resource_group_name = azurerm_resource_group.aks.name

  enabled        = true
  description    = "[${var.project_environment}] Root-Zertifikats-Anpassung im wallbe HUB erkannt"
  data_source_id = azurerm_log_analytics_workspace.aks[0].id
  
  ## Check if there is one warning in the last 2 days if there are more the check has already triggert a message
  query       = <<-QUERY
let containerIds = ContainerInventory | where Image == "wallbe-certificate-cron-service" | distinct ContainerID;
ContainerLog
| where ContainerID in (containerIds)
| where LogEntry has "Root certificate has changed"
| summarize Count = count()
| where Count > 0
| where Count < 2
  QUERY

  severity    = 1
  frequency   = 720
  time_window = 2880
  trigger {
    operator  = "GreaterThan"
    threshold = 0
  }
  action {
    action_group = [azurerm_monitor_action_group.aks[0].id]
  }
}

resource "azurerm_monitor_scheduled_query_rules_alert" "aks-monitor-alt-ocpp-host" {
  count               = var.aks_enable_log_analytics ? 1 : 0
  name                = "${var.project_environment}-monitor-alt-ocpp-host"
  location            = azurerm_resource_group.aks.location
  resource_group_name = azurerm_resource_group.aks.name

  enabled        = true
  description    = "Alert if there have been logins on alt ocpp host"
  data_source_id = azurerm_log_analytics_workspace.aks[0].id

  query       = <<-QUERY
let containerIds = ContainerInventory | where Image == "wallbe-ocpp-service" | distinct ContainerID;
ContainerLog
| where ContainerID in (containerIds)
| where LogEntry has "Login over alternative endpoint detected"
| summarize Count = count()
| where Count > 0
  QUERY

  severity    = 1
  frequency   = 1440
  time_window = 2880
  trigger {
    operator  = "GreaterThan"
    threshold = 0
  }
  action {
    action_group = [azurerm_monitor_action_group.aks[0].id]
  }
}

resource "azurerm_virtual_network" "aks" {
  name                = "${var.project_name}-${var.project_environment}-cluster-vnet"
  resource_group_name = azurerm_resource_group.aks.name
  location            = azurerm_resource_group.aks.location
  address_space       = [var.aks_vnet_address_space]
}

resource "azurerm_subnet" "aks" {
  name                 = "aks-subnet"
  resource_group_name  = azurerm_resource_group.aks.name
  virtual_network_name = azurerm_virtual_network.aks.name
  address_prefixes     = [var.aks_vnet_address_space]
  service_endpoints    = ["Microsoft.Storage"]
}

resource "azurerm_kubernetes_cluster" "aks" {
  name                    = "${var.project_name}-${var.project_environment}-cluster"
  resource_group_name     = azurerm_resource_group.aks.name
  location                = azurerm_resource_group.aks.location
  sku_tier                = var.aks_sku
  dns_prefix              = "${var.project_name}-${var.project_environment}"
  kubernetes_version      = var.aks_k8s_version
  node_resource_group     = "${var.project_name}-${var.project_environment}-cluster-resources"
  private_cluster_enabled = false

  identity {
    type = "SystemAssigned"
  }

  default_node_pool {
    name                 = "nodepool1"
    node_count           = var.aks_node_count
    vm_size              = var.aks_node_vm_size
    os_disk_size_gb      = var.aks_node_osdisk_size
    type                 = "VirtualMachineScaleSets"
    vnet_subnet_id       = azurerm_subnet.aks.id
    orchestrator_version = var.aks_k8s_node_version
    upgrade_settings {
      max_surge = "1"
    }
  }

  role_based_access_control {
    enabled = true
  }

  addon_profile {
    kube_dashboard {
      enabled = false
    }
    oms_agent {
      enabled = var.aks_enable_log_analytics
      log_analytics_workspace_id = (var.aks_enable_log_analytics ? azurerm_log_analytics_workspace.aks[0].id : null)
    }
  }

  network_profile {
    network_plugin = "kubenet"
    load_balancer_profile {
      idle_timeout_in_minutes = 20
      outbound_ip_address_ids = [azurerm_public_ip.aks-outbound.id]
    }
  }
}

resource "azurerm_managed_disk" "neo4j-data" {
  name                 = "${var.project_name}-${var.project_environment}-neo4j-data"
  resource_group_name  = azurerm_resource_group.aks.name
  location             = azurerm_resource_group.aks.location
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "64"
  tags = {
    snapshotRequired   = var.storage_create_snapshots
  }
}

resource "azurerm_role_assignment" "neo4j-data_contributor" {
  scope                = azurerm_managed_disk.neo4j-data.id
  role_definition_name = "Contributor"
  principal_id         = azurerm_kubernetes_cluster.aks.identity[0].principal_id
}

resource "azurerm_managed_disk" "hivemq-data" {
  name                 = "${var.project_name}-${var.project_environment}-hivemq-data"
  resource_group_name  = azurerm_resource_group.aks.name
  location             = azurerm_resource_group.aks.location
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "64"
}

resource "azurerm_role_assignment" "hivemq-data_contributor" {
  scope                = azurerm_managed_disk.hivemq-data.id
  role_definition_name = "Contributor"
  principal_id         = azurerm_kubernetes_cluster.aks.identity[0].principal_id
}

resource "azurerm_managed_disk" "matomo-data" {
  count                = (var.project_environment == "production" || var.project_environment == "staging") ? 1 : 0
  name                 = "${var.project_name}-${var.project_environment}-matomo-data"
  resource_group_name  = azurerm_resource_group.aks.name
  location             = azurerm_resource_group.aks.location
  storage_account_type = "Premium_LRS"
  create_option        = "Empty"
  disk_size_gb         = "64"
}

resource "azurerm_role_assignment" "matomo-data_contributor" {
  count                = (var.project_environment == "production" || var.project_environment == "staging") ? 1 : 0
  scope                = azurerm_managed_disk.matomo-data[0].id
  role_definition_name = "Contributor"
  principal_id         = azurerm_kubernetes_cluster.aks.identity[0].principal_id
}

resource "azurerm_storage_account" "common" {
  name                      = replace("${var.project_name}-${var.project_environment}", "-", "")
  resource_group_name       = azurerm_resource_group.aks.name
  location                  = azurerm_resource_group.aks.location
  account_kind              = "StorageV2"
  account_tier              = "Standard"
  account_replication_type  = "ZRS"
  access_tier               = "Hot"
  enable_https_traffic_only = true
  min_tls_version           = "TLS1_2"
  allow_blob_public_access  = false

  network_rules {
    default_action             = "Deny"
    ip_rules                   = var.storage_account_granted_ips
    virtual_network_subnet_ids = [azurerm_subnet.aks.id]
  }
}

resource "azurerm_storage_share" "document-share" {
  name = "document-share"
  storage_account_name = azurerm_storage_account.common.name
  quota = 32
}

resource "kubernetes_config_map" "nginx-ingress-controller" {
  metadata {
    name      = "nginx-ingress-controller"
    namespace = "kube-system"
  }

  data = {
    enable-vts-status             = "false"
    server-tokens                 = "false"
    proxy-hide-headers            = "Server"
    enable-underscores-in-headers = "true"
    ssl-ciphers                   = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:AES256-SHA256"
    ssl-protocols                 = "TLSv1.1 TLSv1.2 TLSv1.3"
  }
}

resource "helm_release" "nginx-ingress" {
  name      = "nginx-ingress"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart     = "ingress-nginx"
  version   = "4.0.6"
  namespace = "kube-system"
  replace   = true
  wait      = false

  set {
    name  = "controller.ingressClassResource.name"
    value = "nginx"
  }
  set {
    name  = "controller.ingressClass"
    value = "nginx"
  }
  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.aks.ip_address
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-resource-group"
    value = azurerm_public_ip.aks.resource_group_name
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-tcp-idle-timeout"
    value = 20
  }
  set {
    name  = "controller.extraArgs.default-ssl-certificate"
    value = "default/${var.project_name}-${var.project_environment}-cert-secret"
  }
  set {
    name  = "controller.kind"
    value = "DaemonSet"
  }
  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }

  depends_on = [
    kubernetes_config_map.nginx-ingress-controller
  ]
}


resource "kubernetes_config_map" "nginx-ingress-controller-ocpp-1-6" {
  count                           = var.project_environment == "playground" ? 0 : 1
  metadata {
    name      = "nginx-ingress-controller-ocpp-1-6"
    namespace = "kube-system"
  }

  data = {
    enable-vts-status             = "false"
    server-tokens                 = "false"
    proxy-hide-headers            = "Server"
    enable-underscores-in-headers = "true"
    ssl-ciphers                   = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:AES256-SHA256"
    ssl-protocols                 = "TLSv1.1 TLSv1.2 TLSv1.3"
  }
}

resource "helm_release" "nginx-ingress-ocpp-1-6" {
  count     = var.project_environment == "playground" ? 0 : 1
  name      = "nginx-ingress-ocpp-1-6"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart     = "ingress-nginx"
  version   = "4.0.6"
  namespace = "kube-system"
  replace   = true
  wait      = false

  set {
    name  = "controller.ingressClassResource.name"
    value = "nginx-ocpp-1-6"
  }
  set {
    name  = "controller.ingressClass"
    value = "nginx-ocpp-1-6"
  }
  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.aks-ocpp-1-6[0].ip_address
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-resource-group"
    value = azurerm_public_ip.aks-ocpp-1-6[0].resource_group_name
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-tcp-idle-timeout"
    value = 20
  }
  set {
    name  = "controller.extraArgs.default-ssl-certificate"
    value = "default/endpoint-ocpp-1-6-cert-secret"
  }
  set {
    name  = "controller.kind"
    value = "DaemonSet"
  }
  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }

  depends_on = [
    kubernetes_config_map.nginx-ingress-controller-ocpp-1-6
  ]
}

resource "kubernetes_config_map" "nginx-ingress-controller-ocpp-1-6-alt" {
  count                           = var.project_environment == "playground" ? 0 : 1
  metadata {
    name      = "nginx-ingress-controller-ocpp-1-6-alt"
    namespace = "kube-system"
  }

  data = {
    enable-vts-status             = "false"
    server-tokens                 = "false"
    proxy-hide-headers            = "Server"
    enable-underscores-in-headers = "true"
    ssl-ciphers                   = "ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-RSA-AES256-SHA:AES256-SHA256"
    ssl-protocols                 = "TLSv1.1 TLSv1.2 TLSv1.3"
  }
}

resource "helm_release" "nginx-ingress-ocpp-1-6-alt" {
  count     = var.project_environment == "playground" ? 0 : 1
  name      = "nginx-ingress-ocpp-1-6-alt"
  repository = "https://kubernetes.github.io/ingress-nginx"
  chart     = "ingress-nginx"
  version   = "4.0.6"
  namespace = "kube-system"
  replace   = true
  wait      = false

  set {
    name  = "controller.ingressClassResource.name"
    value = "nginx-ocpp-1-6-alt"
  }

  set {
    name  = "controller.ingressClass"
    value = "nginx-ocpp-1-6-alt"
  }
  set {
    name  = "controller.service.loadBalancerIP"
    value = azurerm_public_ip.aks-ocpp-1-6-alt[0].ip_address
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-resource-group"
    value = azurerm_public_ip.aks-ocpp-1-6-alt[0].resource_group_name
  }
  set {
    name  = "controller.service.annotations.service\\.beta\\.kubernetes\\.io/azure-load-balancer-tcp-idle-timeout"
    value = 20
  }
  set {
    name  = "controller.extraArgs.default-ssl-certificate"
    value = "default/endpoint-ocpp-1-6-alt-cert-secret"
  }
  set {
    name  = "controller.kind"
    value = "DaemonSet"
  }
  set {
    name  = "controller.service.externalTrafficPolicy"
    value = "Local"
  }

  depends_on = [
    kubernetes_config_map.nginx-ingress-controller-ocpp-1-6-alt
  ]
}

resource "helm_release" "cert-manager" {
  name       = "cert-manager"
  chart      = "cert-manager"
  repository = "https://charts.jetstack.io"
  version    = "1.6.0"
  namespace  = "kube-system"
  replace    = true

  set {
    name  = "ingressShim.defaultIssuerName"
    value = "letsencrypt-production"
  }

  set {
    name  = "ingressShim.defaultIssuerKind"
    value = "ClusterIssuer"
  }
}

resource "random_password" "matomo_mysql_server_admin_password" {
  count            = (var.project_environment == "production" || var.project_environment == "staging") ? 1 : 0
  length           = 16
  special          = true
  override_special = "_%@"
}

# Create a MySQL Server
resource "azurerm_mysql_server" "matomo" {
  count                             = (var.project_environment == "production" || var.project_environment == "staging") ? 1 : 0
  name                              = "${var.project_name}-${var.project_environment}-matomo-mysql-server"
  location                          = azurerm_resource_group.aks.location
  resource_group_name               = azurerm_resource_group.aks.name
 
  administrator_login               = var.matomo_mysql_admin_login

  # even if marked for change in terraform plan the password is not changed unless explicitly destroyed and recreaded
  administrator_login_password      = random_password.matomo_mysql_server_admin_password[0].result
 
  sku_name                          = var.matomo_mysql_sku_name
  version                           = var.matomo_mysql_version
 
  storage_mb                        = var.matomo_mysql_storage
  auto_grow_enabled                 = true
  
  backup_retention_days             = var.project_environment == "production" ? 30 : 7
  geo_redundant_backup_enabled      = false

  public_network_access_enabled     = true
  ssl_enforcement_enabled           = true
  ssl_minimal_tls_version_enforced  = "TLS1_2"
}

resource "azurerm_mysql_firewall_rule" "matomo" {
  count               = (var.project_environment == "production" || var.project_environment == "staging") ? 1 : 0
  name                = "${var.project_name}-${var.project_environment}-matomo-mysql-rule"
  resource_group_name = azurerm_resource_group.aks.name
  server_name         = azurerm_mysql_server.matomo[0].name
  start_ip_address    = azurerm_public_ip.aks-outbound.ip_address
  end_ip_address      = azurerm_public_ip.aks-outbound.ip_address
}

# Create a MySQL Database
resource "azurerm_mysql_database" "matomo" {
  count               = (var.project_environment == "production" || var.project_environment == "staging") ? 1 : 0
  name                = var.matomo_mysql_db_name
  resource_group_name = azurerm_resource_group.aks.name
  server_name         = azurerm_mysql_server.matomo[0].name
  charset             = "utf8"
  collation           = "utf8_unicode_ci"
}

# Output MySQL password
#output "matomo_mysql_server_admin_password" {
#  description = "The MySQL password is:"
#  value = random_password.matomo_mysql_server_admin_password.result
#}