azure_tenant_id                  = "e8bd4ad0-8137-4049-8a74-71426cdde849"

azure_subscription_id            = "8f25cf1f-907b-4f96-8d5f-f6d831f28007"

azure_location                   = "West Europe"

project_name                     = "wallbe-hub"

project_environment              = "development"

aks_sku                          = "Free"

aks_k8s_version                  = "1.21.2"

aks_k8s_node_version             = "1.21.2"

aks_node_count                   = 2

aks_node_osdisk_size             = 64

aks_node_vm_size                 = "Standard_D4as_v4"

aks_vnet_address_space           = "10.220.0.0/16"

aks_enable_log_analytics         = false

log_analytics_retention_in_days  = 30

storage_account_granted_ips      = []

storage_create_snapshots         = false

monitoring_business_incidents_webhook = "none.local"
