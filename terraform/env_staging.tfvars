azure_tenant_id                       = "e8bd4ad0-8137-4049-8a74-71426cdde849"

azure_subscription_id                 = "492b15fe-9e0b-429e-8bb5-61d391647ab7"

azure_location                        = "West Europe"

project_name                          = "wallbe-hub"

project_environment                   = "staging"

aks_sku                               = "Free"

aks_k8s_version                       = "1.19.7"

aks_k8s_node_version                  = "1.19.7"

aks_node_count                        = 2

aks_node_osdisk_size                  = 64

aks_node_vm_size                      = "Standard_D4as_v4"

aks_vnet_address_space                = "10.230.0.0/16"

aks_enable_log_analytics              = true

log_analytics_retention_in_days       = 30

storage_account_granted_ips           = []

storage_create_snapshots              = true

matomo_mysql_version                  = "8.0"

matomo_mysql_sku_name                 = "B_Gen5_1"

matomo_mysql_storage                  = "5120"

monitoring_business_incidents_webhook = "https://o365adessogroup.webhook.office.com/webhookb2/57dae572-440d-40a7-84c5-0be25993adbe@3d355765-67d9-47cd-9c7a-bf31179f56eb/IncomingWebhook/00e560c43c064d5e956d71ec31d4d256/4651529c-812f-432c-aaac-c4fd8bd25e09"
