azure_tenant_id                  = "e8bd4ad0-8137-4049-8a74-71426cdde849"

azure_subscription_id            = "6c2fa45c-bec1-432b-b404-559d23c1e8a3"

azure_location                   = "West Europe"

project_name                     = "wallbe-hub"

project_environment              = "playground"

aks_sku                          = "Free"

aks_k8s_version                  = "1.21.2"

aks_k8s_node_version             = "1.21.1"

aks_node_count                   = 3

aks_node_osdisk_size             = 32

aks_node_vm_size                 = "Standard_D4as_v4"

aks_vnet_address_space           = "10.210.0.0/16"

aks_enable_log_analytics         = false

log_analytics_retention_in_days  = 30

storage_account_granted_ips      = []

storage_create_snapshots         = false

monitoring_business_incidents_webhook = "none.local"
