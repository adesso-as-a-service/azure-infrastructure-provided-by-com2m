variable "azure_tenant_id" {
  type = string
}

variable "azure_subscription_id" {
  type = string
}

variable "azure_location" {
  type    = string
  default = "West Europe"
}

variable "project_name" {
  type = string
}

variable "project_environment" {
  type = string
}

variable "aks_sku" {
  type = string
}

variable "aks_k8s_version" {
  type = string
}

variable "aks_k8s_node_version" {
  type = string
}

variable "aks_node_count" {
  type = number
}

variable "aks_node_osdisk_size" {
  type = number
}

variable "aks_node_vm_size" {
  type = string
}

variable "aks_vnet_address_space" {
  type = string
}

variable "aks_enable_log_analytics" {
  type = bool
}

variable "log_analytics_retention_in_days" {
  type = number
}

variable "storage_account_granted_ips" {
  type = list(string)
}

variable "storage_create_snapshots" {
  type = bool
}

variable "matomo_mysql_admin_login" {
  type        = string
  description = "Login to authenticate to MySQL Server"
  default     = "matomo-admin"
}

variable "matomo_mysql_db_name" {
  type    = string
  description = "MySQL Server database name for matomo"
  default = "matomo"
}

variable "matomo_mysql_version" {
  type        = string
  description = "MySQL Server version to deploy"
  default     = "8.0"
}

variable "matomo_mysql_sku_name" {
  type        = string
  description = "MySQL SKU: Basic - Gen5 - 1 Core"
  default     = "B_Gen5_1"
}

variable "matomo_mysql_storage" {
  type        = string
  description = "MySQL Storage in MB, from 5120 MB to 4194304 MB"
  default     = "5120"
}

variable "monitoring_business_incidents_webhook" {
  type        = string
  description = "Webhook for business incidents"
}