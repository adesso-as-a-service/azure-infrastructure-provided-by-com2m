#!/bin/bash
# use pattern: mongodb+srv://<cluster>/<db>
# e.g.: mongodb+srv://wallbe-hub-dev-cluster.x40hy.mongodb.net/core
MONGODB_URI="<MONGODB_URI>"
MONGO_USER="<USERNAME>"
MONGO_PASS="<PASSWORD>"

MONGO_SCRIPTS=(
  "WALLBE2-2003_mongodb_0.js"
  "WALLBE2-2003_mongodb_1_1.js"
  "WALLBE2-2003_mongodb_1_2.js"
  "WALLBE2-2003_mongodb_2_1.js"
  "WALLBE2-2003_mongodb_2_2.js"
  "WALLBE2-2003_mongodb_3_1.js"
  "WALLBE2-2003_mongodb_3_2.js"
  "WALLBE2-2003_mongodb_4_1.js"
  "WALLBE2-2003_mongodb_4_2.js"
  "WALLBE2-2003_mongodb_5_1.js"
  "WALLBE2-2003_mongodb_5_2.js"
)

for script in "${MONGO_SCRIPTS[@]}" ; do
    echo "############################################"
    echo "Press enter to continue with script: $script"
    echo "############################################"
    read
    mongo $MONGODB_URI \
        -u $MONGO_USER \
        -p $MONGO_PASS \
        < "js-scripts/$script"
    echo
done

echo "Press enter to exit..."
read
