# ACHTUNG!
Das Skript wurde auf einem Mac entwickelt und getestet. Andere Systeme werden nicht unterstützt.

___

# Anleitung
1. Man benötigt `cypher-shell` und `mongo` im Pfad, um die Migrationsskripte durchzuführen.
2. Via brew installieren:
    ```
    brew install cypher-shell
    brew install mongodb-community-shell
    ```
3. In `WALLBE2-2003_step_1.sh` müssen die Credentials und Verbindungsinformationen für die Neo4j angepasst werden.
4. In `WALLBE2-2003_step_2.sh` müssen die Credentials und Verbindungsinformationen für die MongoDB angepasst werden.
5. Das Skript `js-scripts/WALLBE2-2003_mongodb_0.js` ggf. anpassen, je nachdem ob die Collections existieren oder nicht.
6. Anschließend die beiden Skripte nacheinander ausführen.
