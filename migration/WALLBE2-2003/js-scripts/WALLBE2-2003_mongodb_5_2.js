db.getCollection('globalStatisticsOperatingStatusByDay').find().sort({ "timestamp" : 1 }).limit(1).forEach(document =>{
    document.totalBefore = { provisioning:  NumberLong(0), operational:  NumberLong(0), archived:  NumberLong(0)}

    db.getCollection('globalStatisticsOperatingStatusByDay').save(document);
});

db.getCollection('globalStatisticsOperatingStatusByDay').find().sort({ "timestamp" : 1 }).skip(1).forEach(document => {
    db.getCollection('globalStatisticsOperatingStatusByDay').find({"timestamp": { $lt: document.timestamp}}).sort({"timestamp": -1}).limit(1).forEach( previousDocument =>{
        document.totalBefore = {
            provisioning:  previousDocument.totalBefore.provisioning + previousDocument.counts.provisioning,
            operational: previousDocument.totalBefore.operational +  previousDocument.counts.operational,
            archived:  previousDocument.totalBefore.archived +  previousDocument.counts.archived
        }
    db.getCollection('globalStatisticsOperatingStatusByDay').save(document);
    });
});
