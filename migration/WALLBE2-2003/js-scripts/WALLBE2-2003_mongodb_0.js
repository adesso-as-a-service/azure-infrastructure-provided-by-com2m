// db.getCollection('globalStatisticsContractsByDay').remove({});
// db.getCollection('globalStatisticsHardwareByDay').remove({});
// db.getCollection('globalStatisticsTenantsByDay').remove({});
// db.getCollection('globalStatisticsUserAccountsByDay').remove({});
//
// db.getCollection('globalStatisticsContractsByHour').remove({});
// db.getCollection('globalStatisticsHardwareByHour').remove({});
// db.getCollection('globalStatisticsUserAccountsByHour').remove({});
// db.getCollection('globalStatisticsTenantsByHour').remove({});
//
// db.getCollection('statisticsContractsByDay').remove({});
// db.getCollection('statisticsHardwareByDay').remove({});
// db.getCollection('statisticsTenantsByDay').remove({});
// db.getCollection('statisticsUserAccountsByDay').remove({});
//
// db.getCollection('statisticsContractsByHour').remove({});
// db.getCollection('statisticsHardwareByHour').remove({});
// db.getCollection('statisticsUserAccountsByHour').remove({});
// db.getCollection('statisticsTenantsByHour').remove({});

// ALTERNATIV: Falls die Collections noch nie erstellt wurden:
// Collections erstellen
db.createCollection('globalStatisticsContractsByDay', {});
db.createCollection('globalStatisticsHardwareByDay', {});
db.createCollection('globalStatisticsTenantsByDay', {});
db.createCollection('globalStatisticsUserAccountsByDay', {});

db.createCollection('globalStatisticsContractsByHour', {});
db.createCollection('globalStatisticsHardwareByHour', {});
db.createCollection('globalStatisticsUserAccountsByHour', {});
db.createCollection('globalStatisticsTenantsByHour', {});

db.createCollection('statisticsContractsByDay', {});
db.createCollection('statisticsHardwareByDay', {});
db.createCollection('statisticsTenantsByDay', {});
db.createCollection('statisticsUserAccountsByDay', {});

db.createCollection('statisticsContractsByHour', {});
db.createCollection('statisticsHardwareByHour', {});
db.createCollection('statisticsUserAccountsByHour', {});
db.createCollection('statisticsTenantsByHour', {});

// Indexe erstellen
db.getCollection('globalStatisticsContractsByDay').createIndex( {'timestamp': -1} , { unique: true });
db.getCollection('globalStatisticsHardwareByDay').createIndex( {'hardwareClass': 1, 'timestamp': -1} , { unique: true });
db.getCollection('globalStatisticsTenantsByDay').createIndex( {'timestamp': -1} , { unique: true });
db.getCollection('globalStatisticsUserAccountsByDay').createIndex( {'timestamp': -1} , { unique: true });

db.getCollection('globalStatisticsContractsByHour').createIndex( {'timestamp': -1} , { unique: true })
db.getCollection('globalStatisticsHardwareByHour').createIndex( {'hardwareClass': 1, 'timestamp': -1} , { unique: true });
db.getCollection('globalStatisticsUserAccountsByHour').createIndex( {'timestamp': -1} , { unique: true });
db.getCollection('globalStatisticsTenantsByHour').createIndex( {'timestamp': -1} , { unique: true });

db.getCollection('statisticsContractsByDay').createIndex( {'tenantId': 1, 'timestamp': -1} , { unique: true });
db.getCollection('statisticsHardwareByDay').createIndex( {'tenantId': 1, 'hardwareClass': 1, 'timestamp': -1} , { unique: true });
db.getCollection('statisticsTenantsByDay').createIndex( {'tenantId': 1, 'timestamp': -1} , { unique: true });
db.getCollection('statisticsUserAccountsByDay').createIndex( {'tenantId': 1, 'timestamp': -1} , { unique: true });

db.getCollection('statisticsContractsByHour').createIndex( {'tenantId': 1, 'timestamp': -1} , { unique: true });
db.getCollection('statisticsHardwareByHour').createIndex( {'tenantId': 1, 'hardwareClass': 1, 'timestamp': -1} , { unique: true });
db.getCollection('statisticsUserAccountsByHour').createIndex( {'tenantId': 1, 'timestamp': -1} , { unique: true });
db.getCollection('statisticsTenantsByHour').createIndex( {'tenantId': 1, 'timestamp': -1} , { unique: true });
