function getTruncatedByDay(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCHours(0);
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

function getTruncatedByHour(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

var activeContracts = db.getCollection('chargingCurrentContract').aggregate([
{
    "$match": {
        "timestamp": {$lt: new ISODate()},
        "$or": [
            { "terminationTimestamp": {$gt: new ISODate()} },  // future
            { "terminationTimestamp": {$exists: false} },
         ]
    }
},
{
    "$group": {
        "_id" :  0,
        "active": { $sum: 1 },
    }
},
{
    "$project": {
        "_id": 0,
        "active" : "$active",
    }
},
])._batch

var inactiveContracts = db.getCollection('chargingCurrentContract').aggregate([
{
    "$match": {
        "terminationTimestamp": {$lt: new ISODate()}
    }
},
{
    "$group": {
        "_id" :  0,
        "inactive": { $sum: 1 },
    }
},
{
    "$project": {
        "_id": 0,
        "inactive" : "$inactive",
    }
},
])._batch

var active = 0;
var inactive = 0;

if (activeContracts !== undefined && activeContracts.length > 0){
    active = active + activeContracts[0]["active"];
}
if (inactiveContracts !== undefined && inactiveContracts.length > 0){
    inactive = inactive + inactiveContracts[0]["inactive"];
}

db.getCollection('globalStatisticsContractsByDay').insert({
        "timestamp" : getTruncatedByDay(),
        "counts" : {
            "active" : NumberLong(0),
            "inactive" : NumberLong(0)
        },
        "totalBefore" : {
            "active" : NumberLong(active),
            "inactive" : NumberLong(inactive)
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});

db.getCollection('globalStatisticsContractsByHour').insert({
        "timestamp" : getTruncatedByHour(),
        "counts" : {
            "active" : NumberLong(0),
            "inactive" : NumberLong(0)
        },
        "totalBefore" : {
            "active" : NumberLong(active),
            "inactive" : NumberLong(inactive)
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});
