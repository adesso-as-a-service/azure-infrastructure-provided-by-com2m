load('./exports/js/script_2_hardware.js');

var currentOperatingStatusNeo4j = script_2_hardware;

function getTruncatedByDay(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCHours(0);
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

function getTruncatedByHour(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

currentOperatingStatusNeo4j.forEach((statistic) => {
    db.getCollection('statisticsHardwareByDay').insert({
        "tenantId" : statistic["tenantId"],
        "timestamp" : getTruncatedByDay(),
        "hardwareClass": "ReceiptController",
        "counts" : {
            "operational" : NumberLong(0),
            "provisioning" : NumberLong(0)
        },
        "totalBefore" : {
            "operational" : NumberLong(statistic["operational"]),
            "provisioning" : NumberLong(statistic["provisioning"])
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
    });

    db.getCollection('statisticsHardwareByHour').insert({
        "tenantId" : statistic["tenantId"],
        "timestamp" : getTruncatedByHour(),
        "hardwareClass": "ReceiptController",
        "counts" : {
            "operational" : NumberLong(0),
            "provisioning" : NumberLong(0)
        },
        "totalBefore" : {
            "operational" : NumberLong(statistic["operational"]),
            "provisioning" : NumberLong(statistic["provisioning"])
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
    });
});
