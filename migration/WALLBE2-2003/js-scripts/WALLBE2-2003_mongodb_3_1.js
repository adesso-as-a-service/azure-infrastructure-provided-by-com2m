load('./exports/js/script_3_0_tenant.js');

var currentOperatingStatusNeo4j = script_3_0_tenant;

function getTruncatedByDay(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCHours(0);
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

function getTruncatedByHour(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

currentOperatingStatusNeo4j.forEach((statistic) => {
    print(statistic);
    db.getCollection('statisticsTenantsByDay').insert({
        "tenantId" : statistic["tenantId"],
        "timestamp" : getTruncatedByDay(),
        "counts" : {
            "subTenants" : NumberLong(0),
        },
        "totalBefore" : {
            "subTenants" : NumberLong(statistic["subTenants"]),
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
    });

    db.getCollection('statisticsTenantsByHour').insert({
        "tenantId" : statistic["tenantId"],
        "timestamp" : getTruncatedByHour(),
        "counts" : {
            "subTenants" : NumberLong(0),
        },
        "totalBefore" : {
            "subTenants" : NumberLong(statistic["subTenants"]),
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
    });
});
