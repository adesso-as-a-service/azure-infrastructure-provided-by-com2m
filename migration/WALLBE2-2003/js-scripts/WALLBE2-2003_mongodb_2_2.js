load('./exports/js/script_2_hardware.js');

var currentOperatingStatusNeo4j = script_2_hardware;

function getTruncatedByDay(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCHours(0);
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

function getTruncatedByHour(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

var operational = 0;
var provisioning = 0;

currentOperatingStatusNeo4j.forEach((statistic) => {
    operational = operational + statistic["operational"];
    provisioning = provisioning + statistic["provisioning"];
});

db.getCollection('globalStatisticsHardwareByDay').insert({
        "timestamp" : getTruncatedByDay(),
        "hardwareClass": "ReceiptController",
        "counts" : {
            "operational" : NumberLong(0),
            "provisioning" : NumberLong(0)
        },
        "totalBefore" : {
            "operational" : NumberLong(operational),
            "provisioning" : NumberLong(provisioning)
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});

db.getCollection('globalStatisticsHardwareByHour').insert({
        "timestamp" : getTruncatedByHour(),
        "hardwareClass": "ReceiptController",
        "counts" : {
            "operational" : NumberLong(0),
            "provisioning" : NumberLong(0)
        },
        "totalBefore" : {
            "operational" : NumberLong(operational),
            "provisioning" : NumberLong(provisioning)
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});
