load('./exports/js/script_4_0_userAccount.js');
load('./exports/js/script_4_1_userAccount.js');
load('./exports/js/script_4_2_userAccount.js');
load('./exports/js/script_4_3_userAccount.js');
load('./exports/js/script_4_4_userAccount.js');
load('./exports/js/script_4_5_userAccount.js');
load('./exports/js/script_4_6_userAccount.js');
load('./exports/js/script_4_7_userAccount.js');

var tenantsNeo4j = script_4_0_userAccount;
var activeConfirmedNeo4j = script_4_1_userAccount;
var activeConfirmedRoleCustomerNeo4j =  script_4_2_userAccount;
var activeUnconfirmedAndInactiveUnconfirmedNeo4j = script_4_3_userAccount;
var activeUnconfirmedRoleCustomerNeo4j = script_4_4_userAccount;

var inactiveConfirmedNeo4j = script_4_5_userAccount;
var inactiveConfirmedRoleCustomerNeo4j = script_4_6_userAccount;
var inactiveUnconfirmedRoleCustomerNeo4j = script_4_7_userAccount;

function getTruncatedByDay(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCHours(0);
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

function getTruncatedByHour(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

var activeConfirmed = 0;
var activeConfirmedRoleCustomer = 0;
var activeUnconfirmed = 0;
var activeUnconfirmedRoleCustomer = 0;

var inactiveConfirmed = 0;
var inactiveConfirmedRoleCustomer = 0;
var inactiveUnconfirmed = 0;
var inactiveUnconfirmedRoleCustomer = 0;

tenantsNeo4j.forEach((tenant) => {
    const tenantId = tenant["tenantId"];

    var activeConfirmedPerTenant = activeConfirmedNeo4j.find(element => element["tenantId"] === tenantId);
    activeConfirmed += activeConfirmedPerTenant ? activeConfirmedPerTenant["activeConfirmed"] : 0;

    var activeConfirmedRoleCustomerPerTenant = activeConfirmedRoleCustomerNeo4j.find(element => element["tenantId"] === tenantId);
    activeConfirmedRoleCustomer += activeConfirmedRoleCustomerPerTenant ? activeConfirmedRoleCustomerPerTenant["activeConfirmedRoleCustomer"] : 0;

    var activeUnconfirmedPerTenant = activeUnconfirmedAndInactiveUnconfirmedNeo4j.find(element => element["tenantId"] === tenantId);
    activeUnconfirmed += activeUnconfirmedPerTenant ? activeUnconfirmedPerTenant["activeUnconfirmed"] : 0;

    var activeUnconfirmedRoleCustomerPerTenant = activeUnconfirmedRoleCustomerNeo4j.find(element => element["tenantId"] === tenantId);
    activeUnconfirmedRoleCustomer += activeUnconfirmedRoleCustomerPerTenant ? activeUnconfirmedRoleCustomerPerTenant["activeUnconfirmedRoleCustomer"] : 0;

    var inactiveConfirmedPerTenant = inactiveConfirmedNeo4j.find(element => element["tenantId"] === tenantId);
    inactiveConfirmed += inactiveConfirmedPerTenant ? inactiveConfirmedPerTenant["inactiveConfirmed"] : 0;

    var inactiveConfirmedRoleCustomerPerTenant = inactiveConfirmedRoleCustomerNeo4j.find(element => element["tenantId"] === tenantId);
    inactiveConfirmedRoleCustomer += inactiveConfirmedRoleCustomerPerTenant ? inactiveConfirmedRoleCustomerPerTenant["inactiveConfirmedRoleCustomer"] : 0;

    var inactiveUnconfirmedPerTenant = activeUnconfirmedAndInactiveUnconfirmedNeo4j.find(element => element["tenantId"] === tenantId);
    inactiveUnconfirmed += inactiveUnconfirmedPerTenant ? inactiveUnconfirmedPerTenant["inactiveUnconfirmed"] : 0;

    var inactiveUnconfirmedRoleCustomerPerTenant = inactiveUnconfirmedRoleCustomerNeo4j.find(element => element["tenantId"] === tenantId);
    inactiveUnconfirmedRoleCustomer += inactiveUnconfirmedRoleCustomerPerTenant ? inactiveUnconfirmedRoleCustomerPerTenant["inactiveUnconfirmedRoleCustomer"] : 0;
});

db.getCollection('globalStatisticsUserAccountsByDay').insert({
        "timestamp" : getTruncatedByDay(),
        "counts" : {
            "activeConfirmed" : NumberLong(0),
            "activeConfirmedRoleCustomer" : NumberLong(0),
            "activeUnconfirmed" : NumberLong(0),
            "activeUnconfirmedRoleCustomer" : NumberLong(0),
            "inactiveConfirmed" : NumberLong(0),
            "inactiveConfirmedRoleCustomer" : NumberLong(0),
            "inactiveUnconfirmed" : NumberLong(0),
            "inactiveUnconfirmedRoleCustomer" : NumberLong(0),
        },
        "totalBefore" : {
            "activeConfirmed" : NumberLong(activeConfirmed),
            "activeConfirmedRoleCustomer" : NumberLong(activeConfirmedRoleCustomer),
            "activeUnconfirmed" : NumberLong(activeUnconfirmed),
            "activeUnconfirmedRoleCustomer" : NumberLong(activeUnconfirmedRoleCustomer),
            "inactiveConfirmed" : NumberLong(inactiveConfirmed),
            "inactiveConfirmedRoleCustomer" : NumberLong(inactiveConfirmedRoleCustomer),
            "inactiveUnconfirmed" : NumberLong(inactiveUnconfirmed),
            "inactiveUnconfirmedRoleCustomer" : NumberLong(inactiveUnconfirmedRoleCustomer),
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});

db.getCollection('globalStatisticsUserAccountsByHour').insert({
        "timestamp" : getTruncatedByHour(),
        "counts" : {
            "activeConfirmed" : NumberLong(0),
            "activeConfirmedRoleCustomer" : NumberLong(0),
            "activeUnconfirmed" : NumberLong(0),
            "activeUnconfirmedRoleCustomer" : NumberLong(0),
            "inactiveConfirmed" : NumberLong(0),
            "inactiveConfirmedRoleCustomer" : NumberLong(0),
            "inactiveUnconfirmed" : NumberLong(0),
            "inactiveUnconfirmedRoleCustomer" : NumberLong(0),
        },
        "totalBefore" : {
            "activeConfirmed" : NumberLong(activeConfirmed),
            "activeConfirmedRoleCustomer" : NumberLong(activeConfirmedRoleCustomer),
            "activeUnconfirmed" : NumberLong(activeUnconfirmed),
            "activeUnconfirmedRoleCustomer" : NumberLong(activeUnconfirmedRoleCustomer),
            "inactiveConfirmed" : NumberLong(inactiveConfirmed),
            "inactiveConfirmedRoleCustomer" : NumberLong(inactiveConfirmedRoleCustomer),
            "inactiveUnconfirmed" : NumberLong(inactiveUnconfirmed),
            "inactiveUnconfirmedRoleCustomer" : NumberLong(inactiveUnconfirmedRoleCustomer),
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});
