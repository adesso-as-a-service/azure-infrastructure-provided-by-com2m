load('./exports/js/script_5_operatingStatus.js');

var tenantsNeo4j = script_5_operatingStatus;

tenantsNeo4j.forEach((tenant) =>{
    db.getCollection('statisticsOperatingStatusByDay').find({"tenantId": tenant.tenantId }).sort({ "timestamp" : 1 }).limit(1).forEach(document =>{
        document.totalBefore = { provisioning:  NumberLong(0), operational:  NumberLong(0), archived:  NumberLong(0)}

        db.getCollection('statisticsOperatingStatusByDay').save(document);
    });
});
tenantsNeo4j.forEach((tenant) =>{
    db.getCollection('statisticsOperatingStatusByDay').find({"tenantId": tenant.tenantId }).sort({ "timestamp" : 1 }).skip(1).forEach(document => {
        db.getCollection('statisticsOperatingStatusByDay').find({"tenantId": tenant.tenantId, "timestamp": { $lt: document.timestamp}}).sort({"timestamp": -1}).limit(1).forEach( previousDocument =>{
            document.totalBefore = {
                provisioning:  previousDocument.totalBefore.provisioning + previousDocument.counts.provisioning,
                operational: previousDocument.totalBefore.operational +  previousDocument.counts.operational,
                archived:  previousDocument.totalBefore.archived +  previousDocument.counts.archived
            }
            db.getCollection('statisticsOperatingStatusByDay').save(document);
        });
    });
});
