load('./exports/js/script_1_contract.js');

var tenantsNeo4j = script_1_contract;

function getTruncatedByDay(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCHours(0);
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

function getTruncatedByHour(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

var activeContracts = db.getCollection('chargingCurrentContract').aggregate([
{
    //active contracts
    "$match": {
        "timestamp": {$lt: new ISODate()},
        "$or": [
            { "terminationTimestamp": {$gt: new ISODate()} },  // future
            { "terminationTimestamp": {$exists: false} },
         ]
    }
},
{
    "$group": {
        "_id" :  "$tenantId",
        "active": { $sum: 1 },
        "tenantId": { "$first": "$tenantId" },
    }
},
{
    "$project": {
        "_id": 0,
        "tenantId": "$tenantId",
        "active" : "$active",
    }
},
])._batch

var inactiveContracts = db.getCollection('chargingCurrentContract').aggregate([
{
    //inactive contracts
    "$match": {
        "terminationTimestamp": {$lt: new ISODate()}
    }
},
{
    "$group": {
        "_id" :  "$tenantId",
        "inactive": { $sum: 1 },
        "tenantId": { "$first": "$tenantId" },
    }
},
{
    "$project": {
        "_id": 0,
        "tenantId": "$tenantId",
        "inactive" : "$inactive",
    }
},
])._batch

tenantsNeo4j.forEach((tenant) => {
     const tenantId = tenant["tenantId"];
     var active = 0;
     var inactive = 0;

     var activeObj = activeContracts.find(element => element["tenantId"] === tenantId);
     if (typeof activeObj != "undefined") {
         print(activeObj["active"]);
           active = active + activeObj["active"];
     }

     var inactiveObj = inactiveContracts.find(element => element["tenantId"] === tenantId);
     if (typeof inactiveObj != "undefined") {
           inactive = inactive + inactiveObj["inactive"];
     }

     db.getCollection('statisticsContractsByDay').insert({
        "tenantId" : tenantId,
        "timestamp" : getTruncatedByDay(),
        "counts" : {
            "active" : NumberLong(0),
            "inactive" : NumberLong(0)
        },
        "totalBefore" : {
            "active" : NumberLong(active),
            "inactive" : NumberLong(inactive)
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
     });

     db.getCollection('statisticsContractsByHour').insert({
        "tenantId" : tenantId,
        "timestamp" : getTruncatedByHour(),
        "counts" : {
            "active" : NumberLong(0),
            "inactive" : NumberLong(0)
        },
        "totalBefore" : {
            "active" : NumberLong(active),
            "inactive" : NumberLong(inactive)
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
     });
});
