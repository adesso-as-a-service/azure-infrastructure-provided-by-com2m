load('./exports/js/script_3_1_tenant.js');
load('./exports/js/script_3_2_tenant.js');
load('./exports/js/script_3_3_tenant.js');
load('./exports/js/script_3_4_tenant.js');

function getTruncatedByDay(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCHours(0);
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

function getTruncatedByHour(){
    var todayTruncated = new ISODate();
    todayTruncated.setUTCMinutes(0);
    todayTruncated.setUTCSeconds(0);
    todayTruncated.setUTCMilliseconds(0);

    return ISODate(todayTruncated.toISOString());
}

var rootTenants = script_3_1_tenant[0].count;
var subTenants = script_3_3_tenant[0].count;
var serviceProviderTenants = script_3_2_tenant[0].count;
var total = script_3_4_tenant[0].count;

db.getCollection('globalStatisticsTenantsByDay').insert({
        "timestamp" : getTruncatedByDay(),
        "counts" : {
            "rootTenants" : NumberLong(0),
            "subTenants" : NumberLong(0),
            "serviceProviderTenants" : NumberLong(0),
            "total" : NumberLong(0),
        },
        "totalBefore" : {
            "rootTenants" : NumberLong(rootTenants),
            "subTenants" : NumberLong(subTenants),
            "serviceProviderTenants" : NumberLong(serviceProviderTenants),
            "total" : NumberLong(total),
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});

db.getCollection('globalStatisticsTenantsByHour').insert({
        "timestamp" : getTruncatedByHour(),
        "counts" : {
            "rootTenants" : NumberLong(0),
            "subTenants" : NumberLong(0),
            "serviceProviderTenants" : NumberLong(0),
            "total" : NumberLong(0),
        },
        "totalBefore" : {
            "rootTenants" : NumberLong(rootTenants),
            "subTenants" : NumberLong(subTenants),
            "serviceProviderTenants" : NumberLong(serviceProviderTenants),
            "total" : NumberLong(total),
        },
        "createdAt" : new ISODate(),
        "lastModifiedAt" : new ISODate()
});
