#!/bin/bash
# neo4j parameters
# use this expression to autmatically get the neo4j pod name: $(kubectl get pods --no-headers -o custom-columns=":metadata.name" | grep neo4j)
NEO4J_POD="<NEO4J_POD>"

# use portforwarding on the neo4j pod and adjust uri if needed
NEO4J_URI="neo4j://localhost:7687"
NEO4J_USER="<USERNAME>"
NEO4J_PASS="<PASSWORD>"

# destination path where json should be exported to
# if changed, the mongodb scripts in 'js-scripts' have to be updated
DESTINATION=exports

# define cypher scripts to execute with format: <filename>; <cypher query>
CYPHER_SCRIPTS=(
  "script_1_contract; MATCH (t:Tenant) WITH t.id as tenantId return tenantId"
  "script_2_hardware; OPTIONAL MATCH (t:Tenant)<-[:parent]-(operational:Hardware {operatingStatus: 'OPERATIONAL'}) OPTIONAL MATCH (t:Tenant)<-[:parent]-(provisioning:Hardware {operatingStatus: 'PROVISIONING'}) WITH t.id as tenantId, count(DISTINCT operational) as operational, count(DISTINCT provisioning) as provisioning return tenantId, operational, provisioning"
  "script_3_0_tenant; OPTIONAL MATCH (t:Tenant) OPTIONAL MATCH (t)<-[:parent]-(subtenant:Tenant) WITH t.id as tenantId, count(DISTINCT subtenant) as subTenants return tenantId, subTenants"
  "script_3_1_tenant; MATCH (t:Tenant) WHERE NOT (t)-[:parent]->(:Tenant) RETURN count(DISTINCT t) AS count"
  "script_3_2_tenant; MATCH (t:Tenant {id:'serviceProvider'}) OPTIONAL MATCH (t)<-[:parent*1..3]-(s:Tenant) RETURN count(DISTINCT t) + count(DISTINCT s) AS count"
  "script_3_3_tenant; MATCH (t:Tenant) WHERE NOT (t)-[:parent]->(:Tenant) OPTIONAL MATCH (t)<-[:parent*1..3]-(s:Tenant) RETURN count(DISTINCT s) AS count"
  "script_3_4_tenant; MATCH (t:Tenant) RETURN count(DISTINCT t) AS count"
  "script_4_0_userAccount; MATCH (t:Tenant) WITH t.id as tenantId return tenantId"
  "script_4_1_userAccount; MATCH (t:Tenant) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userActive: UserAccount {deactivated: false}) WHERE NOT (userActive)-[:has]->(:UserAccountConfirmation) WITH t.id as tenantId, count(DISTINCT userActive) as activeConfirmed return tenantId, activeConfirmed"
  "script_4_2_userAccount; MATCH (t:Tenant) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userActive: UserAccount {deactivated: false})-[:has]->(role:Role {id: 'CUSTOMER'}) WHERE NOT (userActive)-[:has]->(:UserAccountConfirmation) WITH t.id as tenantId, count(DISTINCT userActive) as activeConfirmedRoleCustomer return tenantId, activeConfirmedRoleCustomer"
  "script_4_3_userAccount; MATCH (t:Tenant)<-[:parent]-(userGroup:UserGroup) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userActive: UserAccount {deactivated: false})-[:has]->(:UserAccountConfirmation) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userInactive: UserAccount {deactivated: true})-[:has]->(:UserAccountConfirmation) WITH t.id as tenantId, count(DISTINCT userActive) as activeUnconfirmed, count(DISTINCT userInactive) as inactiveUnconfirmed return tenantId, activeUnconfirmed, inactiveUnconfirmed"
  "script_4_4_userAccount; MATCH (t:Tenant) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userActive: UserAccount {deactivated: false})-[:has]->(role:Role {id: 'CUSTOMER'}) WHERE (userActive)-[:has]->(:UserAccountConfirmation) WITH t.id as tenantId, count(DISTINCT userActive) as activeUnconfirmedRoleCustomer return tenantId, activeUnconfirmedRoleCustomer"
  "script_4_5_userAccount; MATCH (t:Tenant) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userInactive: UserAccount {deactivated: true}) WITH t, userInactive WHERE NOT (userInactive)-[:has]->(:UserAccountConfirmation) WITH t.id as tenantId, count(DISTINCT userInactive) as inactiveConfirmed RETURN tenantId, inactiveConfirmed"
  "script_4_6_userAccount; MATCH (t:Tenant) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userInactive: UserAccount {deactivated: true})-[:has]->(role:Role {id: 'CUSTOMER'}) WHERE NOT (userInactive)-[:has]->(:UserAccountConfirmation) WITH t.id as tenantId, count(DISTINCT userInactive) as inactiveConfirmedRoleCustomer return tenantId, inactiveConfirmedRoleCustomer"
  "script_4_7_userAccount; MATCH (t:Tenant) OPTIONAL MATCH (t)<-[:parent]-(:UserGroup)<-[:parent*0..2]-(userGroup:UserGroup)<-[:parent]-(userInactive: UserAccount {deactivated: true})-[:has]->(role:Role {id: 'CUSTOMER'}) WHERE (userInactive)-[:has]->(:UserAccountConfirmation) WITH t.id as tenantId, count(DISTINCT userInactive) as inactiveUnconfirmedRoleCustomer return tenantId, inactiveUnconfirmedRoleCustomer"
  "script_5_operatingStatus; MATCH (t:Tenant) WITH t.id as tenantId return tenantId"
)

echo "Executing cypher scripts on pod $NEO4J_POD"

# create destination directory if not exists
mkdir -p $DESTINATION

for script in "${CYPHER_SCRIPTS[@]}" ; do
    FILE_NAME=${script%%;*}
    QUERY=${script#*;}
    printf "Saving to %s: %s\n" "$FILE_NAME.json" "$QUERY"

    # execute query and save to file
    cypher-shell \
        -a $NEO4J_URI \
        -u $NEO4J_USER \
        -p $NEO4J_PASS \
        "CALL apoc.export.json.query(\"$QUERY\",\"$FILE_NAME.json\",{})"

    # copy to local machine
    mkdir -p $DESTINATION/json
    kubectl cp "$NEO4J_POD:/var/lib/neo4j/$FILE_NAME.json" "$DESTINATION/json/$FILE_NAME.json"
    # remove file from pod to not waste disk space
    kubectl exec --stdin --tty $NEO4J_POD -- rm "/var/lib/neo4j/$FILE_NAME.json"
    # transform result lines to valid json array
    sed -i '' '1s/^/[/;$!s/$/,/;$s/$/]/' "$DESTINATION/json/$FILE_NAME.json"

    mkdir -p $DESTINATION/js
    # transform json to js variables
    cp "$DESTINATION/json/$FILE_NAME.json" "$DESTINATION/js/$FILE_NAME.js"
    sed -i '' "1s/^/var $FILE_NAME = /;" "$DESTINATION/js/$FILE_NAME.js"
    echo
done

echo "Exported all neo4j results to $DESTINATION. Please continue with script 'WALLBE2-2003_step_2.sh'"
