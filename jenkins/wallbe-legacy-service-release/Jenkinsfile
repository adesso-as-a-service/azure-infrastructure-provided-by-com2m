@Library('wallbe')

def CI_REGISTRY = "docker.com2m.de"
def CI_CREDENTIALS_ID = "com2m-artifactory-jenkins"
def label = getWorkerLabel()
def additionalProperties = [
	parameters([
		choice(name: 'SERVICE', choices: ['log-service', 'payment-receipt-service', 'payment-receipt-ui'], description: 'select one of the services'),
		string(name: "SERVICE_PREFIX", description: "Prefix before the Service name for the Bitbucket Project\n"
		+ "URL is like bitbucket/projects/wallbecloud2/repos/wallbe-core-service\n"
		+ "bitbucket/projects/$BITBUCKET_PROJECT/repos/$SERVICE_PREFIX$SERVICE", defaultValue: "wallbe-"),
		string(name: "BITBUCKET_PROJECT", description: "Name of the bitbucket project (e.g. wallbecloud2).", defaultValue: "wallbecloud2"),
		string(name: "BASE_VERSION", description: "Version that is used to create the release from. Use 'snapshot' if you want to create a release based on the latest upstream or '6.0' to create a hotfix release based on an older release.", defaultValue: "snapshot"),
		string(name: "RELEASE_VERSION", description: "New release version (e.g. 1.0).", defaultValue: "1.0")
	])
]

configureProperties(additionalProperties)
def buildTimestamp

podTemplate(
    label: label,
    containers: [
        containerTemplate(name: "git", image: "bitnami/git:latest", command: "cat", ttyEnabled: true, alwaysPullImage: false, resourceRequestMemory: '128Mi', resourceLimitMemory: '512Mi'),
		containerTemplate(name: "kaniko", image: "gcr.io/kaniko-project/executor:debug", command: "/busybox/cat", ttyEnabled: true, alwaysPullImage: false, resourceRequestMemory: '128Mi', resourceLimitMemory: '512Mi')
    ],
    imagePullSecrets: [
            "com2m-docker-registry-secret"
    ]
) {
    node(label) {
    	def headIsAlreadyTagged = false
    	def sourceDestination = "${CI_REGISTRY}/client/wallbe/${SERVICE_PREFIX}${SERVICE}"

    	withEnv(["JENKINS_AGENT_WORKDIR=${JENKINS_AGENT_WORKDIR}"]) {
    		stage("Checkout") {
  				container("git") {
  					git(url: "https://service.com2m.de/bitbucket/scm/" + env.BITBUCKET_PROJECT + "/" + env.SERVICE_PREFIX + env.SERVICE + ".git", branch: "development", credentialsId: "com2m-bitbucket-jenkins")
  					sh "ls -alh"

  					headIsAlreadyTagged = sh(
  						script: "git describe --exact-match --tags HEAD",
  						returnStatus: true
  					) == 0
  				}
        	}

			stage("Release") {
				container("kaniko") {
					withCredentials([usernamePassword(credentialsId: "com2m-client-wallbe-hub-acr-push", usernameVariable: "REGISTRY_USERNAME", passwordVariable: "REGISTRY_PASSWORD")]) {
						script {
							buildTimestamp = sh(
								script: 'echo -n `date "+%Y%m%d%H%M%S"`',
								returnStdout: true).trim()

							echo "Current build timestamp: $buildTimestamp"

							configureKanikoStage(CI_REGISTRY, CI_CREDENTIALS_ID)

							if (BASE_VERSION == "snapshot") {
								if (headIsAlreadyTagged == 0 ) {
									echo "HEAD is already tagged with stable. Reusing latest stable tag for release."
									def sourceImage = "$sourceDestination:stable"

									sh("echo 'FROM ${sourceImage}' | executor --dockerfile=/dev/stdin --destination=$sourceDestination:legacy-${RELEASE_VERSION}")
									sh("echo 'FROM ${sourceImage}' | executor --dockerfile=/dev/stdin --destination=$sourceDestination:legacy-${RELEASE_VERSION}.$buildTimestamp")
								} else {
									echo "HEAD is beyond latest stable tag, taking latest snapshot for release."
									def snapshotSourceImage = "$sourceDestination:snapshot"
									def sourceImage = "$sourceDestination:stable"

									sh("echo 'FROM ${snapshotSourceImage}' | executor --dockerfile=/dev/stdin --destination=$sourceImage")

									sh("echo 'FROM ${sourceImage}' | executor --dockerfile=/dev/stdin --destination=$sourceDestination:legacy-${RELEASE_VERSION}")
									sh("echo 'FROM ${sourceImage}' | executor --dockerfile=/dev/stdin --destination=$sourceDestination:legacy-${RELEASE_VERSION}.$buildTimestamp")
								}
							} else {
								echo "Taking '${BASE_VERSION}' for release."
								def sourceImage = "$sourceDestination:${BASE_VERSION}"

								sh("echo 'FROM ${sourceImage}' | executor --dockerfile=/dev/stdin --destination=$sourceDestination:legacy-${RELEASE_VERSION}")
								sh("echo 'FROM ${sourceImage}' | executor --dockerfile=/dev/stdin --destination=$sourceDestination:legacy-${RELEASE_VERSION}.$buildTimestamp")
							}
						}
					}
				}
			}

			stage("Tagging") {
				container("git") {
					withCredentials([
						usernamePassword(credentialsId:"com2m-bitbucket-jenkins", usernameVariable: 'GIT_USER', passwordVariable: 'GIT_PASSWORD')
					]) {
						if (BASE_VERSION != "snapshot") {
							versionTag = sh(
								script: "git tag -l ${BASE_VERSION}.* | sort -V | tail -1",
								returnStdout: true
							).trim()

							sh("git checkout ${versionTag}")
						}

						sh("git tag legacy-${RELEASE_VERSION}.${buildTimestamp}")
						sh('git push --tags https://${GIT_USER}:${GIT_PASSWORD}@service.com2m.de/bitbucket/scm/${BITBUCKET_PROJECT}/${SERVICE_PREFIX}${SERVICE}.git')
					}
				}
			}
		}
    }
}