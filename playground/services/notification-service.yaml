apiVersion: apps/v1
kind: Deployment
metadata:
  name: wallbe-notification-service
  labels:
    apps: wallbe-notification-service
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: wallbe-notification-service
  template:
    metadata:
      labels:
        app: wallbe-notification-service
      namespace: default
    spec:
      containers:
        - name: wallbe-notification-service
          image: wallbehub.azurecr.io/wallbe-notification-service:21.6
          imagePullPolicy: Always
          resources:
            limits:
              memory: 512Mi
          ports:
            - containerPort: 7002
          env:
            - name: AZMON_COLLECT_ENV
              value: "False"
            - name: JAVA_OPTS
              value: "-XX:+ExitOnOutOfMemoryError"
            - name: spring.data.mongodb.uri
              valueFrom:
                secretKeyRef:
                  name: mongodb-admin-secret
                  key: uri
            - name: spring.data.mongodb.authentication-database
              value: "admin"
            - name: logging.level.root
              value: "INFO"
            - name: logging.pattern.console
              value: "%clr(%d{${LOG_DATEFORMAT_PATTERN:yyyy-MM-dd HH:mm:ss.SSS}}){faint} %clr(%5p [%X{traceId:-},%X{parentId:-},%X{spanId:-}]) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint}%m %replace(%ex){'[\r\n]',' '}%nopex%n"
            - name: config.encryption.encryptKey
              valueFrom:
                secretKeyRef:
                  name: notification-service-encryption-secret
                  key: encryptKey
            - name: config.encryption.saltHex
              valueFrom:
                secretKeyRef:
                  name: notification-service-encryption-secret
                  key: saltHex
            - name: wallbe.config.oauth.jwt.pubkey
              valueFrom:
                secretKeyRef:
                  name: core-service-jwt-public-key-secret
                  key: public-key
            - name: wallbe.config.clientSecret
              valueFrom:
                secretKeyRef:
                  name: notification-service-client-secret
                  key: secret
            - name: wallbe.config.clientId
              value: "notificationServiceClient"
            - name: wallbe.config.core-service.baseUrl
              value: "http://wallbe-core-service:80"
            - name: wallbe.config.document-service.baseUrl
              value: "http://wallbe-document-service:80"
            - name: wallbe.config.login-ui.baseUrl
              value: "https://{domain}/login"
            - name: wallbe.config.login-ui.legalUrl
              value: ${wallbe.config.login-ui.baseUrl}/legal/legal-notice
            - name: wallbe.config.login-ui.privacyPolicyUrl
              value: ${wallbe.config.login-ui.baseUrl}/legal/privacy-policy
            - name: wallbe.config.mail.enabled
              value: "false"
            - name: wallbe.api-documentation.auth.username
              valueFrom:
                secretKeyRef:
                  name: api-doc-credentials
                  key: username
            - name: wallbe.api-documentation.auth.password
              valueFrom:
                secretKeyRef:
                  name: api-doc-credentials
                  key: password
          volumeMounts:
            - mountPath: /keystores
              name: core-service-jwt-keystore-secret
              readOnly: true
      imagePullSecrets:
        - name: wallbe-hub-docker-registry-secret
      volumes:
        - name: core-service-jwt-keystore-secret
          secret:
            secretName: core-service-jwt-keystore-secret
            defaultMode: 0444
            items:
              - key: store
                path: wallbe.p12
---
apiVersion: v1
kind: Service
metadata:
  name: wallbe-notification-service
  namespace: default
spec:
  ports:
  - port: 80
    targetPort: 7002
  selector:
    app: wallbe-notification-service
