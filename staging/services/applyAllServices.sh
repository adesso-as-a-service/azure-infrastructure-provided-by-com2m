kubectl scale deployment/wallbe-core-service --replicas=0
kubectl scale deployment/wallbe-admin-ui --replicas=0
kubectl scale deployment/wallbe-customer-ui --replicas=0
kubectl scale deployment/wallbe-hubject-service --replicas=0
kubectl scale deployment/wallbe-log-service --replicas=0
kubectl scale deployment/wallbe-login-ui --replicas=0
kubectl scale deployment/wallbe-notification-service --replicas=0
kubectl scale deployment/wallbe-ocpp-service --replicas=0
kubectl scale deployment/wallbe-plugsurfing-service --replicas=0
kubectl scale deployment/wallbe-document-service --replicas=0
kubectl scale deployment/wallbe-job-execution-service --replicas=0
kubectl scale deployment/wallbe-billing-service --replicas=0
kubectl scale deployment/wallbe-payment-receipt-service --replicas=0
kubectl scale deployment/wallbe-payment-receipt-ui --replicas=0
kubectl scale deployment/wallbe-a1-sim-management-service --replicas=0
kubectl scale deployment/wallbe-a1-sim-platform-mock-service --replicas=0
kubectl scale deployment/wallbe-ocpi-service --replicas=0
kubectl scale deployment/wallbe-hivemq --replicas=0
kubectl scale deployment/wallbe-admin-service --replicas=0
kubectl scale deployment/wallbe-computop-service --replicas=0
kubectl scale deployment/wallbe-certificate-cron-service --replicas=0

echo "waiting 10 secs for shutdown"
sleep 10

kubectl apply -f core-service.yaml
kubectl apply -f admin-ui.yaml
kubectl apply -f customer-ui.yaml
kubectl apply -f hubject-service.yaml
kubectl apply -f log-service.yaml
kubectl apply -f login-ui.yaml
kubectl apply -f notification-service.yaml
kubectl apply -f ocpp-service.yaml
kubectl apply -f plugsurfing-service.yaml
kubectl apply -f document-service.yaml
kubectl apply -f job-execution-service.yaml
kubectl apply -f billing-service.yaml
kubectl apply -f payment-receipt-service.yaml
kubectl apply -f payment-receipt-ui.yaml
kubectl apply -f a1-sim-management-service.yaml
kubectl apply -f a1-sim-platform-mock-service.yaml
kubectl apply -f ocpi-service.yaml
kubectl apply -f hivemq.yaml
kubectl apply -f admin-service.yaml
kubectl apply -f computop-service.yaml
kubectl apply -f certificate-cron-service.yaml