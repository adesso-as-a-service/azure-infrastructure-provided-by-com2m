apiVersion: apps/v1
kind: Deployment
metadata:
  name: wallbe-core-service
  labels:
    app: wallbe-core-service
  namespace: default
spec:
  replicas: 1
  selector:
    matchLabels:
      app: wallbe-core-service
  template:
    metadata:
      labels:
        app: wallbe-core-service
      namespace: default
    spec:
      containers:
        - name: wallbe-core-service
          image: wallbehub.azurecr.io/wallbe-core-service:23.0
          resources:
            limits:
              memory: 2Gi
          ports:
            - containerPort: 7000
          env:
            - name: AZMON_COLLECT_ENV
              value: "False"
            - name: JAVA_OPTS
              value: "-XX:+ExitOnOutOfMemoryError"
            - name: spring.neo4j.username
              valueFrom:
                secretKeyRef:
                  name: neo4j-user-secret
                  key: username
            - name: spring.neo4j.password
              valueFrom:
                secretKeyRef:
                  name: neo4j-user-secret
                  key: password
            - name: spring.neo4j.uri
              value: "bolt://neo4j:7687"
            - name: spring.data.mongodb.uri
                  # mongodb+srv://<username>:<password>@wallbe-hub-staging-clus-pri.bo12c.mongodb.net/test?retryWrites=true&w=majority
              valueFrom:
                secretKeyRef:
                  name: mongodb-admin-secret
                  key: uri
            - name: spring.data.mongodb.authentication-database
              value: "admin"
            - name: wallbe.config.service-url
              value: "https://staging.wallbe-hub.com/"
            - name: wallbe.config.user-account-confirmation-ttl
              value: "86400000"
            - name: wallbe.config.reset-password-ttl
              value: "3600000"
            - name: wallbe.config.change-email-confirmation-ttl
              value: "3600000"
            - name: wallbe.config.email-token-expiration-minutes
              value: "5"
            - name: wallbe.config.billing-service.baseUrl
              value: "http://wallbe-billing-service:80"
            - name: wallbe.config.notification-service.baseUrl
              value: "http://wallbe-notification-service:80"
            - name: wallbe.config.document-service.baseUrl
              value: "http://wallbe-document-service:80"
            - name: wallbe.config.log-service.baseUrl
              value: "http://wallbe-log-service:80"
            - name: wallbe.config.sim-management-service.baseUrl
              value: "http://wallbe-a1-sim-management-service:80"
            - name: wallbe.config.login-ui.baseUrl
              value: "https://{domain}/login"
            - name: wallbe.config.oauth.jwt.key-store-password
              valueFrom:
                secretKeyRef:
                  name: core-service-jwt-keystore-secret
                  key: password
            - name: wallbe.config.oauth.jwt.key-store-key-alias
              value: "wallbe"
            - name: wallbe.config.oauth.jwt.key-store-filepath
              value: "/keystores/wallbe.p12"
            - name: wallbe.config.oauth-access-token-expiration-seconds.client-credentials
              value: "86400"
            - name: wallbe.config.matomo.tenantLabelPrefix
              value: "tenant_staging_"
            - name: mqtt.config.host
              value: "wallbe-hivemq"
            - name: mqtt.config.port
              value: "1883"
            - name: logging.level.de.wallbe.core
              value: "DEBUG"
            - name: logging.pattern.console
              value: "%clr(%d{${LOG_DATEFORMAT_PATTERN:yyyy-MM-dd HH:mm:ss.SSS}}){faint} %clr(%5p [%X{traceId:-},%X{parentId:-},%X{spanId:-}]) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint}%m %replace(%ex){'[\r\n]',' '}%nopex%n"
            - name: wallbe.api-documentation.auth.username
              valueFrom:
                secretKeyRef:
                  name: api-doc-credentials
                  key: username
            - name: wallbe.api-documentation.auth.password
              valueFrom:
                secretKeyRef:
                  name: api-doc-credentials
                  key: password
            - name: wallbe.config.encryption.encryptKey
              valueFrom:
                secretKeyRef:
                  name: notification-service-encryption-secret
                  key: encryptKey
            - name: wallbe.config.encryption.saltHex
              valueFrom:
                secretKeyRef:
                  name: notification-service-encryption-secret
                  key: saltHex
            - name: wallbe.security.coreServiceClientSecret
              valueFrom:
                secretKeyRef:
                  name: core-service-client-secret
                  key: secret
            - name: core-service.config.selfBaseUrl
              value: "http://wallbe-core-service:80"
            - name: wallbe.config.general.meterConsistencyCheckEnabled
              value: "false"
            - name: wallbe.config.matomo.enableSync
              value: "true"
            - name: wallbe.config.matomo.baseUrl
              value: "https://staging.wallbe-hub.com/matomo/"
            - name: wallbe.config.matomo.apiToken
              valueFrom:
                secretKeyRef:
                  name: matomo-api-secret
                  key: api-key
          volumeMounts:
            - mountPath: /keystores
              name: core-service-jwt-keystore-secret
              readOnly: true
      volumes:
      - name: core-service-jwt-keystore-secret
        secret:
          secretName: core-service-jwt-keystore-secret
          defaultMode: 0444
          items:
          - key: store
            path: wallbe.p12
      imagePullSecrets:
        - name: wallbe-hub-docker-registry-secret
---
apiVersion: v1
kind: Service
metadata:
  name: wallbe-core-service
  namespace: default
spec:
  ports:
  - port: 80
    targetPort: 7000
  selector:
    app: wallbe-core-service
