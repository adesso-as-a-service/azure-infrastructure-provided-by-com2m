#!/bin/bash

RESOURCE_CLUSTER="wallbe-cloud-staging-cluster"
RESOURCE_GROUP="wallbe-cloud-staging"
RESOURCE_NAME="wallbe-cloud-staging-hivemq-data"

# Obtain the app id of the principal
PRINCIPAL_APP_ID=$(az ad app list --display-name ${RESOURCE_CLUSTER} --query '[].appId' -o tsv)
# Obtain the scope of the database disk
SCOPE="$(az resource show --name ${RESOURCE_NAME} --resource-group ${RESOURCE_GROUP} --resource-type 'Microsoft.Compute/disks'  --query 'id' -o tsv)" 

az role assignment create --assignee ${PRINCIPAL_APP_ID} --role "contributor" --scope ${SCOPE}
