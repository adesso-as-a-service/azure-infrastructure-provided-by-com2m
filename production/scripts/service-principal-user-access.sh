#!/bin/bash
  
# Modify for your environment.
# ACR_NAME: The name of your Azure Container Registry
# ACR_ROLE: One of the following Roles
#   acrpull:     pull only
#   acrpush:     push and pull
#   owner:       push, pull, and assign roles
# SERVICE_PRINCIPAL_NAME: Must be unique within your AD tenant
ACR_NAME=wallbehub
ACR_ROLE=acrpull
SERVICE_PRINCIPAL_NAME=acr-wallbe-hub-aks-user-access
  
# Obtain the full registry ID for subsequent command args
ACR_REGISTRY_ID=$(az acr show --name $ACR_NAME --query id --output tsv)
  
# Create the service principal with rights scoped to the registry.
SP_PASSWD=$(az ad sp create-for-rbac --name http://$SERVICE_PRINCIPAL_NAME --scopes $ACR_REGISTRY_ID --role $ACR_ROLE --query password --output tsv)
SP_APP_ID=$(az ad sp show --id http://$SERVICE_PRINCIPAL_NAME --query appId --output tsv)
  
# Output the service principal's credentials; use these in your services and
# applications to authenticate to the container registry.
echo "Service principal ID: $SP_APP_ID"
echo "Service principal password: $SP_PASSWD"