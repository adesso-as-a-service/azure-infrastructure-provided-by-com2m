
#!/bin/bash
API_USER=doc
API_PASSWORD=docPassword

kubectl create secret generic api-doc-credentials --from-literal=username=${API_USER} --from-literal=password=${API_PASSWORD}
